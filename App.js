import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, View, Platform } from "react-native";
import FusionCharts from "react-native-fusioncharts";
const dataSource = {
     chart: {
         caption: "Market Share of Web Servers",
         plottooltext: "<b>$percentValue</b> of web servers run on $label servers",
         showlegend: "1",
         showpercentvalues: "1",
         legendposition: "bottom",
         usedataplotcolorforlabels: "1",
         theme: "fusion"
     },
     data: [
     {
         label: "Apache",
         value: "32647479"
     },
     {
     label: "Microsoft",
     value: "22100932"
     },
     {
     label: "Zeus",
     value: "14376"
     },
     {
     label: "Other",
     value: "18674221"
     }
    ]
};
export default class App extends Component {
     constructor(props) {
     super(props);
     this.state = dataSource;
         this.libraryPath = Platform.select({
         // Specify fusioncharts.html file location
         ios: require("./assets/fusioncharts.html"),
         android: { uri: "file:///android_asset/fusioncharts.html" }
         });
 }
     render() {
         return (
         <View style={styles.container}>
         <Text style={styles.heading}>
         FusionCharts Integration with React Native
         </Text>
             <View style={styles.chartContainer}>
                 <FusionCharts
                 type={this.state.type}
                 width={this.state.width}
                 height={this.state.height}
                 dataFormat={this.state.dataFormat}
                 dataSource={this.state.dataSource}
                 libraryPath={this.libraryPath} // set the libraryPath property
                 />
             </View>
         </View>
         );
     }
}

const styles = StyleSheet.create({
     container: {
         flex: 1,
         padding: 10
     },
     heading: {
         fontSize: 20,
         textAlign: "center",
         marginBottom: 10
     },
     chartContainer: {
         height: 200
     }
    });
// skip this line if using Create React Native App
AppRegistry.registerComponent("ReactNativeFusionCharts", () => App);